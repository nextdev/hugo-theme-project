let e;

e = $("#app-bar")[0];
const appBar = e? new mdc.topAppBar.MDCTopAppBar(e): undefined;

e = $("#app-drawer")[0];
const navMain = e? new mdc.drawer.MDCDrawer(e): undefined;

$(".mdc-list").each((i, e) => {
    const list = new mdc.list.MDCList(e);
});


if (appBar && navMain) {
    appBar.listen("MDCTopAppBar:nav", () => {
        navMain.open = !navMain.open;
    });
}
